class Driver {
    Driver() {  
    }
    
    public void passBtoA(B b){
        A a= new A();
        a.deleteB(b);
    }
    public static void main(String [] args) {
        Driver d = new Driver();
        B b = new B();
        d.passBtoA(b);
        System.out.println(b);
    }
}