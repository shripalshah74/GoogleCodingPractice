class A {
    int data ;
    public A(){
        data = 5;
    }
    
    public void deleteB(B b){
        System.out.println("from A before deleting object b"+ b);
        b = null;
        System.out.println("from B after deleting object b"+ b);
    }
}