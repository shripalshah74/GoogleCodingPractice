///*
//There are two sorted arrays nums1 and nums2 of size m and n respectively.
//
//Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
//
//Example 1:
//nums1 = [1, 3]
//nums2 = [2]
//
//The median is 2.0
//Example 2:
//nums1 = [1, 2]
//nums2 = [3, 4]
//
//The median is (2 + 3)/2 = 2.5
//*/


import java.util.*;
class MedianOfSortedArrays {
    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        
        
        int l1 = nums1.length;
        int l2 = nums2.length;
        int k = 0; 
        int m = 0;
        Stack<Integer> stack = new Stack <Integer> ();
        System.out.println(l1+l2/2);
        for(int i = 0; i <= (l1+l2)/2 ; i++) {
            if(k >=l1){
		System.out.println(nums2[m]);
                stack.push(nums2[m]);
                m++;
            }                
            else if (m >= l2) {
             	System.out.println(nums1[k]);
		stack.push(nums1[k]);
                k++;
            }
            else {
            if(nums1[k]< nums2[m]) {
             	System.out.println(nums1[k]);
		stack.push(nums1[k]);
                k++;
            }
            else {
		System.out.println(nums2[m]);
                stack.push(nums2[m]);
                m++;
            }
            }   
        }
         if((l1 + l2)%2 == 0){
                int temp1 = stack.pop();
                int temp2 = stack.pop();
                double result = (temp1+temp2)/2.0;
                return  result  ;
            }
            else {
                return stack.pop();
            }
    }
    
    public static void main(String []args) {
        int [] nums1 = new int []{1,2};
        int [] nums2 = new int []{3,4};
        System.out.println(findMedianSortedArrays(nums1, nums2));
    }
}

