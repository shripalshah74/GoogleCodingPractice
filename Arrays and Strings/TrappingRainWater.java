public class TrappingRainWater {
    public static int arr[] = new int [] {0, 1, 0, 23, 1, 0, 1, 3, 2, 1, 25, 1};
    static int findWater(int n) {
        // left[i] contains height of tallest bar to the left of i'th bar including itself
        int left[] = new int[n];
        // right[i] contains height of tallest bar to the right of i'th bar inclding itself
        int right[] = new int[n];
        // Initialize the result
        int water = 0;
        // Fill left array
        left[0] = arr[0];
        for(int i = 1 ; i < n; i ++) {
            left[i] = Math.max(left[i-1], arr[i]);
        }
        
        // Fill right array 
        right[n-1] = arr[n-1];
        for (int i = n-2; i>=0; i--){
            right[i] = Math.max (right[i+1], arr[i]);
        }
        
        for(int i = 0 ; i < n ; i++ ) {
            water = water + Math.min(left[i], right[i]) - arr[i];
        }
        return water;
        
    }
    
    // Driver method to test the function
    public static void main (String [] args) {
        int n = arr.length;
        System.out.println("Maximum water that can be acumulated is : " + findWater(n));
    }
}