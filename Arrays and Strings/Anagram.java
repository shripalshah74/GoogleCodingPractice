import java.util.HashMap;
import java.util.Map;

public class Anagram {
public int sherlockAndAnagrams(String s) {
 
// logic for calling anagram
 
int len = s.length();
int startingIndex = 0;
int startWindow = startingIndex+1;
int lenSample = 1;
int count = 0;
// logic for anagram in the substring after startingIndex
while(startWindow + lenSample <= len) {
while(startWindow + lenSample <= len) {
 
while(startWindow + lenSample <= len) {
if(anagram(s.substring(startingIndex, startingIndex+lenSample), s.substring(startWindow, startWindow+lenSample))) {
count++;
}
startWindow++;
}
startWindow = startingIndex+1;
lenSample++;
}
startingIndex++;
startWindow = startingIndex+1;
lenSample = 1;
}
return count;
}
 
 
 
 
public boolean anagram(String s, String t) {
if(s.length() == t.length()){
// populating the HashMap using the given string
Map<Character,Integer> map = new HashMap<Character, Integer>();
int lenS = s.length();
for(int i = 0; i < lenS; i++ ) {
if(map.containsKey(s.charAt(i))) {
map.put(s.charAt(i), map.get(s.charAt(i))+1);
}
else
{
map.put(s.charAt(i), 1);
}
}
 
// Logic for finding if the substring has an anagram or not.
 
int lenT = t.length();
for(int j = 0; j < lenT; j++) {
if( !map.isEmpty() && map.containsKey(t.charAt(j))) {
if(map.get(t.charAt(j)) ==1) {
map.remove(t.charAt(j));
}
else {
map.put(t.charAt(j), map.get(t.charAt(j))-1);
}
}
else {
return false;
}
}
if(map.isEmpty())
return true;
else return false;
}
else return false;
}
 
public static void main (String [] args) {
Anagram rl = new Anagram();
System.out.println(rl.sherlockAndAnagrams("abcd"));
}
 
}