
import java.util.*;
class Pair {
    int num;
    int count;
    
    public Pair(int num, int count){
        this.num = num;
        this.count = count;
    }
}
// First we will populate the hashmap
// Second we will populate the priority queue
// then well return the list of k most frequent numbers
public class KMostFrequentElements {
    public  List<Integer> topKElement(int [] nums , int k) {
     HashMap<Integer, Integer> hmap = new HashMap();
        for (int num : nums) {
            if(hmap.containsKey(num)) {
                hmap.put(num, hmap.get(num)+1);
            }
            else {
                hmap.put(num, 1);
            }
        }
        
        // create a min heap
        PriorityQueue<Pair> queue = new PriorityQueue<Pair>(new Comparator<Pair>(){
           public int compare(Pair a, Pair b) {
               return a.count - b.count;
           } 
        });
        
        for(Map.Entry<Integer, Integer> entry: hmap.entrySet()) {
            Pair p = new Pair(entry.getKey(), entry.getValue());
            queue.offer(p);
            if(queue.size()>k){
                queue.poll();
            }
        }
        
        // geting elemenets form the queue
        List<Integer> list = new ArrayList<Integer>();
        while(queue.size()>0) {
            list.add(queue.poll().num);
        }
        Collections.reverse(list);
        return list;
        
    }
    public static void main(String [] args) {
        KMostFrequentElements fl = new KMostFrequentElements ();
        List <Integer> list = new ArrayList<Integer> ();
        list = fl.topKElement(new int []{1,1,1,1,1,2,2,3,3,3,4,5,5,6,5,5}, 3);
        for (int lt : list ){
            System.out.println(lt);
        }
    }
}

Successor

public TreeNode successor(TreeNode root, TreeNode p) {
  if (root == null)
    return null;

  if (root.val <= p.val) {
    return successor(root.right, p);
  } else {
    TreeNode left = successor(root.left, p);
    return (left != null) ? left : root;
  }
}
Predecessor

public TreeNode predecessor(TreeNode root, TreeNode p) {
  if (root == null)
    return null;

  if (root.val >= p.val) {
    return predecessor(root.left, p);
  } else {
    TreeNode right = predecessor(root.right, p);
    return (right != null) ? right : root;
  }
}