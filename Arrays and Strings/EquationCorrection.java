//Please use this Google doc to code during your interview. To free your hands for coding, we recommend that you use a headset or a phone with speaker option.
import java.util.*;
class Node {
//    Node parent;
    char c;
    int index;
//    Node child;
//    boolean bad;
        
    Node (char c, int index) {
        this.c = c;
        this.index = index;
//        this.parent = parent;
//        this.child = child;
//        this.bad = bad;
    }
    
}
// [(a+b)+[(s+d)]
// <-]<-)?) ]) ->) = ],  ]<-] = ]] <- ) = ]])?) = ]])-> ]]?] = ]]-> "]" final //result one extra bracket 
class EquationCorrection {
    
    public String getCorrectedEquation ( String s) {

        int i = 0;
        int j=0;
        StringBuilder sb = new StringBuilder(s);
        Stack<Node> stack = new Stack<Node>();
        Node node;
        for(char c : s.toCharArray()) {
            switch (c) {
                case '(' : 
                        node = new Node (')', i-j);
                        stack.push(node);
                    break;
                  
                case '{' : 
                   node = new Node ('}', i-j);
                        stack.push(node);
                    break;
                    
                case '[' : 
                 node = new Node (']', i-j);
                        stack.push(node);
                    break;
                    
                    /////////////////////////////////////////////////////////////////
                    
                case ')' : 
                    if(i==0) {
                        sb.deleteCharAt(i-j);
                     j++;
                    }
                    else if(!stack.isEmpty() && stack.peek().c == c) {                                
                                stack.pop();
                                
                            }
                    else {
                        sb.deleteCharAt(i-j);
                        ++j;
                        
                        
                     }
                    break;
                case ']' :
                      if(i==0) {
                        sb.deleteCharAt(i-j);
                        j++;
                       
                      
                    }
                    else if(!stack.isEmpty() && stack.peek().c == c) {                                
                                stack.pop();  
                            }
                    else {
                        sb.deleteCharAt(i-j);
                        ++j;
                        
                     } 
                    break;
                case '}' :
                      if(i==0) {
                        sb.deleteCharAt(i-j);
                       j++;
                        
                    }
                    else if(!stack.isEmpty() && stack.peek().c == c) {                                
                                stack.pop();
                                
                            }
                    else {
                        sb.deleteCharAt(i-j);
                        ++j; 
                     } break;
              default:break;
                    
            }
           ++i;
        }
	if(stack.isEmpty()){
		return sb.toString();
}
else {
	while(!stack.isEmpty()){
	 node = stack.pop();
	int index = node.index;
	sb.deleteCharAt(index);
	
}
return sb.toString();
}
        
    }

public static void main (String [] args) {
	EquationCorrection ec = new EquationCorrection();
	String s = "{(A*}B{})}}}}]]]]";
	System.out.println(ec.getCorrectedEquation(s));

}
}
