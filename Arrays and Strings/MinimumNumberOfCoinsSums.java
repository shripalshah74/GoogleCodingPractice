public class MinimumNumberOfCoinsSums {

    public int getMinimumNumberOfCoins(int [] coins, int s) {
        int min[] = new int [s+1];
        for(int i = 0 ; i < min.length ; i++) 
            min[i] = 99999 ;
        
        min[0] = 0;
        System.out.println(min[0]);
        for(int i = 1; i <= s; i++){
            for(int j = 0 ; j < coins.length ; j++) {
                if(coins[j]<= i &&  min[i-coins[j]]+1 < min[i])
                    min[i] = min[i-coins[j]] +1;
                    System.out.println(min[i]);
            }
        }
        
        return min[s];
    }
    
    public static void main (String [] args){
        MinimumNumberOfCoinsSums ms = new MinimumNumberOfCoinsSums();
        int s = 3;
        int []coins = new int[]{1,3,5};       
        System.out.println(ms.getMinimumNumberOfCoins(coins, s));
        
    }
}