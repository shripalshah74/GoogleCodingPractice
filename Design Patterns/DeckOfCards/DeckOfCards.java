1import java.util.*;
import java.security.SecureRandom;

class DeckOfCards {
    Card[] deck;
    int currentCard;
    DeckOfCards(){
        String suits[]= {"Ace","Spades","Club","Heart"};
        String faceCards[]= {"2","3","4","5","6","7","8","9","10","Jack", "Queen", "King","Ace"};
        currentCard = 0;
         deck= new Card[52];
            for (int suit = 0; suit< suits.length; suit ++) {
                for (int face = 0; face< faceCards.length; face ++) {
                    deck[face+ suit*13] = new Card(faceCards[face], face + 2, suits[suit]);
                }
            }
    }
    
    public void displayCards() {
        for(Card card: deck) {
            System.out.println(card);
        }  
    }
    
    public void shuffle () {
        SecureRandom random = new SecureRandom();
        for(int i = 0; i < deck.length; i++){
            int second = random.nextInt(52);
            Card temp = deck[i];
            deck[i] = deck[second];
            deck[second] = temp;
            
        }
    }
    
    public static void main (String [] args) {
    DeckOfCards deck = new DeckOfCards();
    deck.displayCards();
    deck.shuffle();
        System.out.println();
        System.out.println("After Shuffle!");
        System.out.println();
    deck.displayCards();
    }
    
}




//D:\Google>java DeckOfCards
//Ace of2 value of the card is 2
//Ace of3 value of the card is 3
//Ace of4 value of the card is 4
//Ace of5 value of the card is 5
//Ace of6 value of the card is 6
//Ace of7 value of the card is 7
//Ace of8 value of the card is 8
//Ace of9 value of the card is 9
//Ace of10 value of the card is 10
//Ace ofJack value of the card is 11
//Ace ofQueen value of the card is 12
//Ace ofKing value of the card is 13
//Ace ofAce value of the card is 14
//Spades of2 value of the card is 2
//Spades of3 value of the card is 3
//Spades of4 value of the card is 4
//Spades of5 value of the card is 5
//Spades of6 value of the card is 6
//Spades of7 value of the card is 7
//Spades of8 value of the card is 8
//Spades of9 value of the card is 9
//Spades of10 value of the card is 10
//Spades ofJack value of the card is 11
//Spades ofQueen value of the card is 12
//Spades ofKing value of the card is 13
//Spades ofAce value of the card is 14
//Club of2 value of the card is 2
//Club of3 value of the card is 3
//Club of4 value of the card is 4
//Club of5 value of the card is 5
//Club of6 value of the card is 6
//Club of7 value of the card is 7
//Club of8 value of the card is 8
//Club of9 value of the card is 9
//Club of10 value of the card is 10
//Club ofJack value of the card is 11
//Club ofQueen value of the card is 12
//Club ofKing value of the card is 13
//Club ofAce value of the card is 14
//Heart of2 value of the card is 2
//Heart of3 value of the card is 3
//Heart of4 value of the card is 4
//Heart of5 value of the card is 5
//Heart of6 value of the card is 6
//Heart of7 value of the card is 7
//Heart of8 value of the card is 8
//Heart of9 value of the card is 9
//Heart of10 value of the card is 10
//Heart ofJack value of the card is 11
//Heart ofQueen value of the card is 12
//Heart ofKing value of the card is 13
//Heart ofAce value of the card is 14
//
//After Shuffle!
//
//Spades of3 value of the card is 3
//Club of10 value of the card is 10
//Club of6 value of the card is 6
//Spades of6 value of the card is 6
//Spades of5 value of the card is 5
//Club of4 value of the card is 4
//Ace of6 value of the card is 6
//Spades ofQueen value of the card is 12
//Club ofJack value of the card is 11
//Heart of3 value of the card is 3
//Heart of8 value of the card is 8
//Ace of10 value of the card is 10
//Spades of4 value of the card is 4
//Spades of9 value of the card is 9
//Heart of4 value of the card is 4
//Ace of5 value of the card is 5
//Spades of8 value of the card is 8
//Ace of3 value of the card is 3
//Heart ofQueen value of the card is 12
//Club of2 value of the card is 2
//Club of3 value of the card is 3
//Heart ofKing value of the card is 13
//Heart of9 value of the card is 9
//Ace ofAce value of the card is 14
//Spades of10 value of the card is 10
//Spades ofJack value of the card is 11
//Club of5 value of the card is 5
//Spades ofKing value of the card is 13
//Heart of2 value of the card is 2
//Ace of7 value of the card is 7
//Heart of10 value of the card is 10
//Ace ofQueen value of the card is 12
//Club of7 value of the card is 7
//Heart of7 value of the card is 7
//Club ofKing value of the card is 13
//Heart of5 value of the card is 5
//Ace of2 value of the card is 2
//Heart of6 value of the card is 6
//Ace of9 value of the card is 9
//Club of9 value of the card is 9
//Club of8 value of the card is 8
//Spades ofAce value of the card is 14
//Ace of4 value of the card is 4
//Heart ofJack value of the card is 11
//Club ofQueen value of the card is 12
//Ace of8 value of the card is 8
//Ace ofJack value of the card is 11
//Ace ofKing value of the card is 13
//Spades of2 value of the card is 2
//Spades of7 value of the card is 7
//Heart ofAce value of the card is 14
//Club ofAce value of the card is 14
//
//D:\Google>

