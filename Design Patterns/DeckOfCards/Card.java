class Card {
    String faceName;
    int faceValue;
    String suit;
    
    Card (String faceName, int faceValue, String suit ) {
        this.faceName = faceName;
        this.faceValue = faceValue;
        this.suit = suit;
    }
    
    public String toString() {
        return suit + " of" + faceName + " value of the card is " + faceValue;
    }
    
}