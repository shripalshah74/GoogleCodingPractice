public class Car {
    private String license;
    private String size;
    private String color;
    Spot spot;
    
    Car(String license, String size, String color){
        this.size = size;
        this.license = license;
        this.color = color;
    }
    
    public String getSize(){
        return size;
    }
    
    public String getLicense() {
        return license;
    }
    
    
    public String toString () {
        return "Car parked with license number:" + license + "color:"+ color + "size:" + size;
    }
}