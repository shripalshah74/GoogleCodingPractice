import java.util.*;
public class ParkingLot {
    int levels;
    Spot[] spots;
    int totalSpots;
    int i = -1;
    
    ParkingLot(int levels, int totalSpots) {
        this.levels = levels;
        this.totalSpots= totalSpots;
        spots = new Spot[totalSpots];
        for (int level = 0 ; level < levels; level++)
        {
            for(int spot=0; spot < totalSpots/levels; spot++){
                spots[spot + level*(totalSpots/levels)] = new Spot(spot + level* totalSpots/levels );
            }
        }
    }
    
    public void displaySpots() {
        for (int level = 0; level < levels ; level++) {
			System.out.println(" Level: "+ level);
            for (int spot =level * (totalSpots/levels); spot < (totalSpots/levels) * (level + 1) ; spot++ ){
                if(spots[spot].available)
                    System.out.printf("\n T");
                else
                    System.out.print("F");
            }
        }
    }
    
    
    public int parkCar(String license, String size, String color ){
        spots[i+1].parkCar(license, size, color);
        i++;
        displaySpots();
        return i;
    }
    
    public boolean removeCar(int spotNo){
        spots[spotNo].removeCar();
        return true;
    }
    
    public static void main(String [] args) {
        ParkingLot parkingLot = new ParkingLot(4, 20);
        parkingLot.displaySpots();
        System.out.println(parkingLot.removeCar(parkingLot.parkCar("MH02MA7455", "Medium", "White")));
        
        
    }
}