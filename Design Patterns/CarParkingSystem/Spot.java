class Spot {
    int spotNo;
    boolean available;
    Car car;
    Spot (int spotNo) {
        this.spotNo = spotNo;
        available = true;
        this.car = null;
    }
    
    public boolean parkCar(String license , String size , String color) {
        car = new Car(license, size, color);
        System.out.println("Car parked at Spot Nomber : " + spotNo+ " is : " + car);
        available = false;
        return true;
    }
    
    public boolean removeCar() {
        System.out.println(car + "REMOVED");
        car = null;
        available = true;
        return true;
    }
}