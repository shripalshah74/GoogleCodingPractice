import java.util.Arrays;
import java.util.concurrent.Callable;

public class AddressFetcher implements Callable<Address> {
	
	Address addressRequest;
	CachedHashSet<Address> addressCache;
	
	public AddressFetcher(CachedHashSet<Address> addressCache, Address addressRequest) {
		this.addressRequest = addressRequest;
		this.addressCache = addressCache;
	}
	
	@Override
	public Address call() throws Exception {
		Address address = null;
		if (addressCache.contains(addressRequest)) {
			System.out.println("It is found in Cache. Getting below info from cache\n"+addressRequest);
			return addressRequest;
		} else {
			address = searchDB(addressRequest.getLines(), addressRequest.getCity(), addressRequest.getState(),
					addressRequest.getPostalCode(), addressRequest.getCountryCode());
			addressCache.add(address);
			return address;
		}
	}
	
	/*
	 * Method for searching the address from the database. Return type is of Address
	 * class
	 */

	/*
	 * You will pass the address to search as the parameters to this method
	 */
	public Address searchDB(String lines, String city, String state, String postalCode, String countryCode) {
		Address address = null;
		
		//Search in DB implementation goes here..
		
		//TODO: Temp code to return adress object when we search DB
		address = CommonAddressCache.createAddress(Arrays.asList(lines.split("\n")), city, state, postalCode, countryCode);
		System.out.println("Getting below info from DB, it is already exisetd in DB\n"+address);
		
		
		//if search returns null
		if(address == null) {
			address = insertAddressToDB(lines, city, state, postalCode, countryCode);
		}
		return address;

	}
	
	private Address insertAddressToDB(String lines, String city, String state, String postalCode, String countryCode) {
		// TODO implement db insertion
		System.out.println("Storing the information in the DB: ");
		return null;
	}

}
