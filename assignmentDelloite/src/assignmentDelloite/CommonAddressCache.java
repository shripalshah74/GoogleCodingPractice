import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CommonAddressCache {

	private static final String STREET = "Street";
	private static final String ISO_COUNTRY_CODE = "isoCountryCode";
	private static final String COUNTRY = "Country";
	private static final String POSTAL_CODE = "PostalCode";
	private static final String STATE = "State";
	private static final String CITY = "City";
	private static final String POSTAL_ADDRESS = "PostalAddress";
	private static final int CACHE_CAPACITY = 100;
	private static final int THREADPOOL_CAPACITY = 4;
	private static final CachedHashSet<Address> addressCache = new CachedHashSet<Address>(CACHE_CAPACITY);
	ExecutorService executor = Executors.newFixedThreadPool(THREADPOOL_CAPACITY);

	/*
	 * This method is used to construct address object from all details
	 */
	public static Address createAddress(List<String> streets, String city, String state, String postalCode,
			String countryCode) {

		Address address = new Address();
		StringBuffer street = new StringBuffer();
		int streetSize = streets.size();
		for (int i = 0; i < streetSize; i++) {
			street.append(streets.get(i));
			if (i != streetSize - 1) {
				street.append("\n");
			}
		}
		address.setLines(street.toString());
		address.setCity(city);
		address.setState(state);
		address.setPostalCode(postalCode);
		address.setCountryCode(countryCode);
		return address;

	}

	/*
	 * this method is for getting the address from the db. So in this, you firstly
	 * need to check if the address is in the cache which I willl be designing. If
	 * it is present in the cache, then return it from there. else query the
	 * searchDB method. If the addess returned is null, then create the address by
	 * calling the create address method
	 * 
	 * 
	 * public Address getAddress(String lines, String city, String state, String
	 * postalCode, String countryCode) { Address address = null; return address; }
	 */

	public List<Address> parseXml(String fileLocation) {
		List<Address> addressList = new ArrayList<Address>();
		try {
			List<Callable<Address>> callables = new ArrayList<Callable<Address>>();
			File fXmlFile = new File(fileLocation);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			// TODO: delete when we want to do DTD validation
			dBuilder.setEntityResolver(new EntityResolver() {
				@Override
				public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
					if (systemId.contains("http://svcdev6.ariba.com/schemas/cXML/1.2.032/InvoiceDetail.dtd")) {
						return new InputSource(new StringReader(""));
					} else {
						return null;
					}
				}
			});

			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			NodeList addresses = doc.getElementsByTagName(POSTAL_ADDRESS);
			for (int temp = 0; temp < addresses.getLength(); temp++) {
				List<String> streetsList = new ArrayList<String>();
				String city = null;
				String state = null;
				String postalCode = null;
				String countryCode = null;

				Node address = addresses.item(temp);
				if (address.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) address;
					NodeList streets = eElement.getElementsByTagName(STREET);
					if (streets != null) {
						for (int j = 0; j < streets.getLength(); j++) {
							Element line = (Element) streets.item(j);
							if (line != null) {
								String street = line.getTextContent();
								if (street != null && street.trim() != "") {
									streetsList.add(street.trim());
								}
							}
						}
					}

					Node cityNode = getFirstElementByName(eElement, CITY);
					if (cityNode != null) {
						city = cityNode.getTextContent().trim();
					}
					Node stateNode = getFirstElementByName(eElement, STATE);
					if (stateNode != null) {
						state = stateNode.getTextContent().trim();
					}
					Node pCodeNode = getFirstElementByName(eElement, POSTAL_CODE);
					if (pCodeNode != null) {
						postalCode = pCodeNode.getTextContent().trim();
					}
					Node countryNode = getFirstElementByName(eElement, COUNTRY);
					if (countryNode != null) {
						countryCode = countryNode.getAttributes().getNamedItem(ISO_COUNTRY_CODE).getTextContent()
								.trim();
					}

					/*
					 * System.out.println(" Street  combined: " + streetsList);
					 * System.out.println("City : " + city); System.out.println("State : " + state);
					 * System.out.println("Postal Code : " + postalCode);
					 * System.out.println("Country Code : " + countryCode);
					 */
					Address addressObj = createAddress(streetsList, city, state, postalCode, countryCode);

					callables.add(new AddressFetcher(addressCache, addressObj));

				}
			}
			executor.invokeAll(callables).stream().map(future -> {
				try {
					if (future.isDone())
						return future.get();
					else
						throw new Exception("future is not complete");
				} catch (Exception e) {
					throw new IllegalStateException(e);
				}
			}).forEach(addressList::add);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return addressList;
	}

	private Node getFirstElementByName(Element parentElement, String nodeName) {
		return parentElement.getElementsByTagName(nodeName).item(0);
	}

	public static void main(String[] args) {
		String locFile = "./invoice.xml";
		CommonAddressCache addressCache = new CommonAddressCache();
		List<Address> addressList = addressCache.parseXml(locFile);
		List<Address> addressList1 = addressCache.parseXml(locFile);
		addressCache.executor.shutdown();
	}

}
