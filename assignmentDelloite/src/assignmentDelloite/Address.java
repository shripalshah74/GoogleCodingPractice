
public class Address {

	public String city;
	public String state;
	public String postalCode;
	public String countryCode;
	public String lines;

	public Address() {	}

	public Address(String city, String state, String postalCode, String countryCode, String lines){
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.countryCode = countryCode;
		this.lines = lines;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getLines() {
		return lines;
	}

	public void setLines(String lines) {
		this.lines = lines;
	}

	@Override
	public boolean equals(Object obj1) {
		if(obj1 instanceof Address) {
			Address ad1 = (Address) obj1;
		return this.lines.equalsIgnoreCase(ad1.lines)
				&& this.city.equalsIgnoreCase(ad1.city)
				&& this.state.equalsIgnoreCase(ad1.state)
				&& this.postalCode.equalsIgnoreCase(ad1.postalCode)
				&& this.countryCode.equalsIgnoreCase(ad1.countryCode);
		}
		else 
			return super.equals(obj1);
	}
	
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + lines.hashCode();
        result = 31 * result + city.hashCode();
        result = 31 * result + state.hashCode();
        result = 31 * result + postalCode.hashCode();
        result = 31 * result + countryCode.hashCode();
		return result;
	}
	
	public String toString() {
		return lines+"\n"
				+city+"\n"
				+state+"\n"
				+postalCode+"\n"
				+countryCode+"\n";
	}
	
}