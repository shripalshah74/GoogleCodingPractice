package assignmentDelloite;
import java.util.LinkedHashMap;
import java.util.Map;


public class CachedHashMap<K,V> extends LinkedHashMap<K,V> {
	int cacheSize;
	
	public CachedHashMap(int cacheSize) {
		super(16, 0.75f, true);
		this.cacheSize = cacheSize;
	}
	
	public V add(K element, V value) {
		if(!this.containsKey(element)) {
			return this.get(element);
		}
		 this.put(element, value);
		return value;
	}

	protected boolean removeEldestEntry(Map.Entry eldest) {
        return size() > cacheSize;
     }

	public static void main(String[]args) {
		CachedHashMap <String, String> chm = new CachedHashMap<String,String>(100);
		chm.add("zeel", "value");
		System.out.println(chm.get("zeel"));
	}
}

	

