
http://www.jewellibrary.com
https://www.padmavatijewellery.com
www.al-haseen.com
import java.util.*;

/**
 * Given input string (ex. "khanandanagaraj") and dictionary (ex. ["nandan","anand","khan"]),
 * return input string by adding bold (<b> </b>) tags for matching words in dictionary.
 * Have single bold tag for overlapping words (ex. "<b>khanandan</b>agaraj").
 */
public class WordsHighlighter {
    Node dict = new Node('*');
    public static void main(String args[]) {
        String input = "khanandannagaraja";
        List<String> dictWords = Arrays.asList("khan","nandan","anand","raj");
        WordsHighlighter mc = new WordsHighlighter();
        mc.constructDict(dictWords);
        System.out.println(mc.getBoldWords(input));
    }

    public String getBoldWords(String input) {
        boolean tagStarted = false;
        int firstStartIndex = -1;
        int lastSuccessIndex = -1;
        StringBuilder sb = new StringBuilder();
        List<Node> runningWords = new ArrayList();
        char[] charArray = input.toCharArray();

        for(int j=0;j<charArray.length;j++) {
            char c = charArray[j];

            // 1. check existing words flow
            List<Node> nodesToRemove = new ArrayList();
            for(int i=0;i<runningWords.size();i++) {
                Node rw = runningWords.get(i);
                if(rw.isLeafNode()) {
                    nodesToRemove.add(rw);
                    lastSuccessIndex = sb.length();
                    continue;
                }
                Node childNode = rw.getChildNode(c);
                if(childNode != null) {
                    runningWords.set(i, childNode);
                }
                else {
                    nodesToRemove.add(rw);
                }

            }
            runningWords.removeAll(nodesToRemove);

            // 2. check for new word to begin
            Node n = dict.getChildNode(c);
            if(n != null) {
                runningWords.add(n);
            }

            // 3. Handle adding bold tag
            if(!tagStarted && runningWords.size() > 0) {
                firstStartIndex = (sb.length() == 0)?0:sb.length()+1;
                tagStarted = true;
                sb.append(c+"");
            }
            else
            if(tagStarted && ((runningWords.size() == 0)
                || (j+1 == charArray.length && runningWords.size() == 1))) {
                sb.append(c+"");
                if((j+1 == charArray.length && runningWords.size() == 1 && runningWords.get(0).isLeafNode())) {
                    lastSuccessIndex = sb.length();
                }
                if(lastSuccessIndex!= -1 && firstStartIndex != -1) {
                    sb.insert(lastSuccessIndex,"</b>");
                    sb.insert(firstStartIndex, "<b>");
                    lastSuccessIndex = -1;
                    firstStartIndex = -1;
                }
                else {
                    firstStartIndex = -1;
                }
                tagStarted = false;
            }
            else {
                sb.append(c+"");
            }
        }
        return sb.toString();
    }


    public void constructDict(List<String> dictWords) {
        for(String word: dictWords) {
            Node parent = dict;
            for(char c: word.toCharArray()) {
                Node charNode = parent.getChildNode(c);
                if(charNode == null) {
                    charNode = new Node(c);
                }
                parent.addChildNode(c,charNode);
                parent = charNode;
            }
        }
    }

}

class Node {
    char c;
    HashMap<Character,Node> childNodes;
    public Node(char c) {
        childNodes = new HashMap();
        this.c = c;
    }
    public Node getChildNode(char c) {
        return childNodes.get(c);
    }
    public void addChildNode(char c, Node node) {
        childNodes.put(c,node);
    }
    public boolean isLeafNode() {
        return childNodes.size() == 0;
    }

    @Override
    public String toString() {
        return "[("+c+") ->"+childNodes.toString()+"]";
    }
}