import java.util.*;
public class TVKeyboard {
    static String word = "KUNAL";
    static int s= 5;
    static HashMap  <Character, Integer>hmap = new HashMap<Character, Integer>();
    public static void formHashMap() {
        int i = 0;
        for (char ch = 'A'; ch <= 'Z'; ++ch) 
            hmap.put(ch, ++i); 
    }
    public static void getSteps() {
        System.out.println("Given string : " + word);
        int length  = word.length();
        int i = 0;
        int currentPosition = 1;
        int futurePosition;
        int currentDepth;
        int currentHorizontal;
        int futureDepth;
        int futureHorizontal;
        char[] charact = word.toCharArray();
        while (i < length) { 
            
            System.out.println("*******************************************CURRENT POSITION " + currentPosition);
            // get future position of the cursor
                futurePosition = getfuturePosition(charact[i]);
            // get currentHorizontal
                currentHorizontal = getCurrentHorizontalPosition(currentPosition);
            // get current Depth
                currentDepth = getCurrentDepth(currentPosition);
            
            // get futureHorizontal
                futureHorizontal = getFutureHorizontal(futurePosition);
            
            // get futureDepth
                futureDepth = getFutureDepth(futurePosition);
            
            // for Horizontal movement
            if(Math.abs(currentHorizontal - futureHorizontal) >0){
                if(currentHorizontal< futureHorizontal){
                         for(int j = 0; j< Math.abs(futureHorizontal-currentHorizontal); j++){
                            System.out.println("Right");
                        }
                    
                    }
                else{
                     for(int j = 0; j< Math.abs(futureHorizontal-currentHorizontal); j++){
                            System.out.println("Left");
                    }
                    
                     
                }
              
            }
            
            
            // for vertical movement
            if(Math.abs(currentDepth - futureDepth)>0){
             //   System.out.println("Depth difference"+ Math.abs(currentDepth - futureDepth));
                if(currentDepth< futureDepth){
                         for(int j = 0; j< Math.abs(futureDepth-currentDepth); j++){
                            System.out.println("Down");
                        }
                     
                    }
                else{
                     for(int j = 0; j<Math.abs(futureDepth-currentDepth); j++){
                            System.out.println("Up");
                        }
                    
                }
              
            }
            
             System.out.println("***OK***");
            i++;
            currentPosition = futurePosition;
            
        }
    }
    
// get future position of the cursor
    public static int getfuturePosition(char charact){
        int futurePosition = hmap.get(charact);
        //  System.out.println("FuturePosition" + futurePosition);
        return futurePosition;
    }
// get currentHorizontal
    public static int getCurrentHorizontalPosition(int currentPosition){
        int currentHorizontal = currentPosition % s;
        //System.out.println("CurrentHorizontal: " + currentHorizontal);
        return currentHorizontal;
    }
// get current Depth
    public static int  getCurrentDepth(int currentPosition){
        int currentDepth = currentPosition / s;
        //System.out.println("CurrentDepth: " + currentDepth);
        return currentDepth;
    }

// get futureHorizontal
   public static int getFutureHorizontal(int futurePosition){
       int futureHorizontal = futurePosition % s;
       //System.out.println("FutureHoriontal: " + futureHorizontal);
       return futureHorizontal;
   }


// get futureDepth
    public static int  getFutureDepth( int futurePosition){
        int futureDepth = futurePosition / s;
        //System.out.println("FutureDepth: " + futureDepth);
        return futureDepth;
    }
 public static void main (String [] args ){
     formHashMap();
     getSteps();
 }
    
}

//
//a| b c d e f g h i j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z
//1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26
/*    
for height
current = 1 
currentLevel = current/5
destination 2
destination / 5 = destinationlevel 
travelHeight = destination level -(current level)
    
    
for horizontal movement 
positionOfNext / 5 -1

  */  





//========================================================================================================================

package test;

/*Please use this Google doc to code during your interview. To free your hands for coding, we recommend that you use a headset or a phone with speaker option.

        Hello, good morning



        A B C D E
        F G H I J
        K L M N O
        P Q R S T
        U V W X Y
        Z


        up, down, left, right, select


        input ( avengers, width: 5)

        A->S (1)->(19) 19/5 -> ceil(3)=4 - 19%5 => 4*/

import java.util.*;

enum Action {
    up, down, left, right, select;
}

public class GoogleSolution {
    public List<Action> solution(String input, int width) {
        char arr[] = input.toLowerCase().toCharArray();
        char currentChar = 'a';
        List<Action> result = new ArrayList();
        for(int i=0;i<arr.length;i++) {
            char c = arr[i];
            if(c != currentChar ) {
                Position currentPos = getLetterPosition(currentChar, width);
                Position destPos = getLetterPosition(c, width);
                result.addAll(generateSteps(currentPos, destPos));
            }
            currentChar = c;
            result.add(Action.select);
        }
        return result;
    }


    public List<Action> generateSteps(Position src, Position desc) {
        int xDiff = desc.x - src.x;
        int yDiff = desc.y - src.y;
        Action yAction = (yDiff < 0)? Action.left: Action.right;
        Action xAction = (xDiff < 0)? Action.up: Action.down;
        List<Action> result = new ArrayList();
        /*TODO: instead of looping seperately, loop xDiff and yDiff together until they become zero and keep checking if move is valid */
        for(int i=0;i<Math.abs(xDiff);i++) {
            result.add(xAction);
        }
        for(int i=0;i<Math.abs(yDiff);i++) {
            result.add(yAction );
        }
        return result;
    }


    public Position getLetterPosition(char c, int width) {
        int charNum = ((int)c)-96; //ex: a=1, z=26
        int x = (int)Math.ceil(((double)charNum)/5.0);
        int y = (charNum%5 == 0)?5*x:charNum%5;
        return new Position(x,y);
    }

    public static void main(String args[]) {
        GoogleSolution gs = new GoogleSolution();
        System.out.println(gs.solution("avengers", 5));
    }

}

class Position {
    int x,y;
    public Position(int x,int y) {
        this.x = x;
        this.y = y;
    }
}
